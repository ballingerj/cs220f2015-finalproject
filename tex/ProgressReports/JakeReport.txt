Jake Ballinger
CMPSC 220
Fall 2015
Dr. Roos
30 November 2015

Dear Dr. Roos,

Katherine and I have made tremendous progress with our project. We've tackled the basic syntax of Scala with a simple HelloWorld.scala program, and I've included detailed comments on the syntax of Scala and how to write this simplest of programs. 

In addition, we've looked at some of the features of Scala and have been deliberating to narrow down a focus. We've decided to impement as many of the essential basics as possible, with a focus on both the style of Scala (in terms of syntax and "scala programs" vs. "java programs"). 

Some future things to look forward to, for your programmer's pleasure:
--> Currying
--> More object-oriented shenanigans
--> Squence Comprehensions
--> XML Processing
--> Pattern Matching
--> etc!

We have the goal of writing one program per day this week. To be determined as to whether or not this will actually happen, but it's a goal. 

We're still unhappy about not being allowed from using the Shakespearian language, but I understand your reservations. 

Sincerely, 
Jake Ballinger

P.S. - We're really excited about the cookies. 
